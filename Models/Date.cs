﻿using System;

namespace CourseWork.Models
{
    [Serializable]
    internal sealed class Date
    {
        #region Validation

        private static bool ValidateYear(int year)
        {
            return year >= 1900 && year <= 2100;
        }

        private static bool ValidateMonth(int month)
        {
            return month >= 1 && month <= 12;
        }

        private static bool ValidateDay(int day)
        {
            return day >= 1 && day <= 31;
        }

        #endregion

        public Date(int year, int month, int day)
        {
            Year = year;
            Month = month;
            Day = day;
        }

        private int _year;

        public int Year
        {
            get { return _year; }
            private set
            {
                if (!ValidateYear(value))
                {
                    _year = DateTime.Now.Year;

                    return;
                }

                _year = value;
            }
        }

        private int _month;

        public int Month
        {
            get { return _month; }
            private set
            {
                if (!ValidateMonth(value))
                {
                    _month = DateTime.Now.Month;

                    return;
                }

                _month = value;
            }
        }

        private int _day;

        public int Day
        {
            get { return _day; }
            private set
            {
                if (!ValidateDay(value))
                {
                    _day = DateTime.Now.Day;

                    return;
                }

                _day = value;
            }
        }

        public static bool operator ==(Date a, Date b)
        {
            if (ReferenceEquals(a, b))
            {
                return true;
            }

            if (((object)a) == null || ((object)b) == null)
            {
                return false;
            }

            return a.Equals(b);
        }

        public static bool operator !=(Date a, Date b)
        {
            return !(a == b);
        }

        public override string ToString()
        {
            return $"{Year}-{Month}-{Day}";
        }

        public override bool Equals(object obj)
        {
            return obj != null && obj.GetHashCode() == GetHashCode();
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Year;
                hashCode = (hashCode*397) ^ Month;
                hashCode = (hashCode*397) ^ Day;
                return hashCode;
            }
        }
    }
}
