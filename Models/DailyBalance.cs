﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CourseWork.Models
{
    internal sealed class DailyBalance : IDailyBalance
    {
        private readonly Date _date;
        private readonly List<IOrder> _orders;

        public DailyBalance(Date date)
        {
            _date = date;

            _orders = new List<IOrder>();
        }

        #region Validation

        private void ValidateOrder(IOrder order)
        {
            if (order == null)
            {
                throw new ArgumentException("Order can't be null", nameof(order));
            }

            if (order.OrderDate != _date)
            {
                throw new ArgumentException("Order's date must be the same as the balance date");
            }
        }

        #endregion

        public Date GetDate()
        {
            return _date;
        }

        public void AddOrder(IOrder order)
        {
            ValidateOrder(order);

            _orders.Add(order);
        }

        public void RemoveOrder(IOrder order)
        {
            ValidateOrder(order);

            _orders.Remove(order);
        }

        public IList<IOrder> GetOrders()
        {
            return _orders.AsReadOnly();
        }

        public decimal GetTotal()
        {
            return _orders.Sum(i => i.GetTotal());
        }

        public Dictionary<Waiter, decimal> GetWaitersStats()
        {
            return _orders.GroupBy(i => i.Waiter).ToDictionary(i => i.Key, j => j.Sum(o => o.GetTotal()));
        }

        public void PrintStats(Action<List<IOrder>> statsCreator)
        {
            statsCreator(_orders);
        }

        public IOrder this[int index]
        {
            get
            {
                return _orders[index];
            }
        }
    }
}
