﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseWork.Models
{
    internal sealed class Order : IOrder
    {
        private readonly List<Dish> _dishes;

        public Waiter Waiter { get; private set; }
        public Date OrderDate { get; private set; }

        public Order(Waiter waiter)
        {
            if (waiter == null)
            {
                throw new ArgumentException("Waiter can't be null", nameof(waiter));
            }

            _dishes = new List<Dish>();

            Waiter = waiter;
            OrderDate = new Date(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        }

        public void AddDish(Dish dish)
        {
            if (dish == null)
            {
                throw new ArgumentException("Dish can't be null", nameof(dish));
            }

            _dishes.Add(dish);
        }

        public void RemoveDish(Dish dish)
        {
            if (dish == null)
            {
                throw new ArgumentException("Dish can't be null", nameof(dish));
            }

            _dishes.Remove(dish);
        }

        public IList<Dish> GetDishes()
        {
            return _dishes.AsReadOnly();
        }

        public decimal GetTotal()
        {
            return _dishes.Sum(i => i.Price);
        }

        public decimal GetTips()
        {
            return GetTotal()*0.15m;
        }

        public decimal GetTotalWithTips()
        {
            return GetTotal() + GetTips();
        }

        public Dish this[int index]
        {
            get
            {
                return _dishes[index];
            }
        }

        public override string ToString()
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(OrderDate.ToString());
            stringBuilder.AppendLine(Waiter.ToString());

            stringBuilder.AppendLine("\nDishes:");

            for (var i = 0; i < _dishes.Count; i++)
            {
                stringBuilder.AppendLine(this[i].ToString());
            }

            stringBuilder.AppendLine($"\nTotal: {GetTotal():c}");
            stringBuilder.AppendLine($"Tips: {GetTips():c}");
            stringBuilder.AppendLine($"\nTotal with tips: {GetTotalWithTips():c}");

            return stringBuilder.ToString();
        }
    }
}
