﻿using System;

namespace CourseWork.Models
{
    [Serializable]
    internal sealed class DishType
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set
            {
                if (!Validate(value))
                {
                    _name = string.Empty;

                    return;
                }

                _name = value;
            }
        }

        public DishType(string name)
        {
            Name = name;
        }

        #region Validation

        private static bool Validate(string name)
        {
            return !string.IsNullOrEmpty(name.Trim());
        }

        #endregion

        public override string ToString()
        {
            return Name;
        }
    }
}
