﻿using System;
using System.Collections.Generic;

namespace CourseWork.Models
{
    internal interface IDailyBalance
    {
        Date GetDate();
        void AddOrder(IOrder order);
        void RemoveOrder(IOrder order);
        IList<IOrder> GetOrders();
        decimal GetTotal();
        Dictionary<Waiter, decimal> GetWaitersStats();
        void PrintStats(Action<List<IOrder>> statsCreator);
        IOrder this[int index] { get; }
    }
}