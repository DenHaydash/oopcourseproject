﻿using System.Collections.Generic;

namespace CourseWork.Models
{
    internal interface IOrder
    {
        Waiter Waiter { get; }
        Date OrderDate { get; }
        void AddDish(Dish dish);
        void RemoveDish(Dish dish);
        IList<Dish> GetDishes();
        decimal GetTotal();
        decimal GetTips();
        decimal GetTotalWithTips();
        Dish this[int index] { get; }
    }
}