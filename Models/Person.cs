﻿using System;

namespace CourseWork.Models
{
    [Serializable]
    internal class Person
    {
        public Person(string firstName, string lastName, Date birthday)
        {
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthday;
        }

        private Date _birthday;

        public Date Birthday
        {
            get { return _birthday; }
            private set
            {
                if (value == null)
                {
                    throw new ArgumentException("Birthday can't be null");
                }

                _birthday = value;
            }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            private set
            {
                if (string.IsNullOrEmpty(value.Trim()))
                {
                    _firstName = "unknown";

                    return;
                }

                _firstName = value;
            }
        }

        private string _lastName;

        public string LastName
        {
            get { return _lastName; }
            private set
            {
                if (string.IsNullOrEmpty(value.Trim()))
                {
                    _lastName = "unknown";

                    return;
                }

                _lastName = value;
            }
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName} ({Birthday})";
        }
    }
}