﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CourseWork.Models
{
    [Serializable]
    internal sealed class Dish
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            private set
            {
                if (string.IsNullOrEmpty(value.Trim()))
                {
                    _name = "unknown";

                    return;
                }

                _name = value;
            }
        }

        private decimal _price;

        public decimal Price
        {
            get { return _price; }
            private set
            {
                if (value < 0)
                {
                    _price = 0;

                    return;
                }

                _price = value;
            }
        }

        private DishType _type;

        public DishType Type
        {
            get { return _type; }
            private set
            {
                if (value == null)
                {
                    _type = new DishType("unknown");

                    return;
                }

                _type = value;
            }
        }

        public Dish(string name, decimal price, DishType type)
        {
            var validationResult = Validate(name, price, type);

            foreach (var invalidField in validationResult.Where(i => !i.Value))
            {
                throw new ArgumentException($"Wrong value for field {invalidField.Key}", invalidField.Key);
            }

            Name = name;
            Price = price;
            Type = type;
        }

        #region Validation

        public static Dictionary<string, bool> Validate(string name, decimal price, DishType type)
        {
            return new Dictionary<string, bool>()
            {
                {"name", !string.IsNullOrEmpty(name.Trim()) },
                {"price", price >= 0  },
                {"type", type != null }
            };
        }

        #endregion

        public override string ToString()
        {
            return $"{Name} ({Type}) - {Price:C}";
        }
    }
}
