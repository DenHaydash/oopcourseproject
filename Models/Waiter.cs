﻿using System;

namespace CourseWork.Models
{
    [Serializable]
    internal sealed class Waiter : Person
    {
        public WaiterCategory Category { get; private set; }

        public Waiter(string firstName, string lastName, Date birthday, WaiterCategory category) : base(firstName, lastName, birthday)
        {
            Category = category;
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName} (Category: {Category})";
        }
    }
}
