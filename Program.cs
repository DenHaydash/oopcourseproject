﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using CourseWork.Context;
using CourseWork.Helpers;
using CourseWork.Models;
using CourseWork.Watchers;

namespace CourseWork
{
    internal class Program
    {
        private static readonly string BasePath;

        private static readonly IContext<Waiter> WaitersContext;
        private static readonly IContext<Dish> DishesContext;
        private static readonly IContext<DishType> DishTypesContext;

        private static readonly IDailyBalance DailyBalance;

        static Program()
        {
            BasePath = @"c:\cw";

            DailyBalance = new DailyBalance(new Date(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));

            string pathToSavedWaiters = $@"{BasePath}\data\waiters.bin";
            WaitersContext =
                new Context<Waiter>(SerializationHelper.ReadDataFromBinaryFile<List<Waiter>>(pathToSavedWaiters));
            WaitersContext.Subscribe(new ContextWatcher<List<Waiter>>(pathToSavedWaiters));

            string pathToSavedDishes = $@"{BasePath}\data\dishes.bin";
            DishesContext = new Context<Dish>(SerializationHelper.ReadDataFromBinaryFile<List<Dish>>(pathToSavedDishes));
            DishesContext.Subscribe(new ContextWatcher<List<Dish>>(pathToSavedDishes));

            string pathToSavedDishesTypes = $@"{BasePath}\data\dishTypes.bin";
            DishTypesContext =
                new Context<DishType>(SerializationHelper.ReadDataFromBinaryFile<List<DishType>>(pathToSavedDishesTypes));
            DishTypesContext.Subscribe(new ContextWatcher<List<DishType>>(pathToSavedDishesTypes));
        }

        private static void PrintTable<T>(IList<T> items)
        {
            #region TableSymbols

            const string cellLeftTop = "┌";
            const string cellRightTop = "┐";
            const string cellLeftBottom = "└";
            const string cellRightBottom = "┘";
            const string cellHorizontalJointTop = "┬";
            const string cellHorizontalJointbottom = "┴";
            const string cellVerticalJointLeft = "├";
            const string cellTJoint = "┼";
            const string cellVerticalJointRight = "┤";
            const string cellHorizontalLine = "─";
            const string cellVerticalLine = "│";

            #endregion

            if (!items.Any())
            {
                Console.WriteLine("\nNo data to display");

                return;
            }

            const int tableSize = 48;

            for (var i = 0; i <= tableSize; i++)
            {
                if (i == 0)
                {
                    Console.Write(cellLeftTop);
                    continue;
                }

                if (i == 9)
                {
                    Console.Write(cellHorizontalJointTop);
                }

                if (i == tableSize)
                {
                    Console.Write(cellRightTop);
                    continue;
                }

                Console.Write(cellHorizontalLine);
            }
            Console.WriteLine();

            Console.Write(cellVerticalLine);
            Console.Write($"Id\t {cellVerticalLine} Item".PadRight(44));
            Console.Write(cellVerticalLine);
            Console.WriteLine();

            var counter = 0;

            foreach (var item in items)
            {
                for (var i = 0; i <= tableSize; i++)
                {
                    if (i == 0)
                    {
                        Console.Write(cellVerticalJointLeft);
                        continue;
                    }

                    if (i == 9)
                    {
                        Console.Write(cellTJoint);
                    }

                    if (i == tableSize)
                    {
                        Console.Write(cellVerticalJointRight);
                        continue;
                    }

                    Console.Write(cellHorizontalLine);
                }
                Console.WriteLine();

                Console.Write(cellVerticalLine);
                Console.Write($"{++counter}\t {cellVerticalLine} {item}".PadRight(43));
                Console.Write(cellVerticalLine);
                Console.WriteLine();
            }

            for (var i = 0; i <= tableSize; i++)
            {
                if (i == 0)
                {
                    Console.Write(cellLeftBottom);
                    continue;
                }

                if (i == 9)
                {
                    Console.Write(cellHorizontalJointbottom);
                }

                if (i == tableSize)
                {
                    Console.Write(cellRightBottom);
                    continue;
                }

                Console.Write(cellHorizontalLine);
            }
            Console.WriteLine();
        }

        private static void PrintMenu(string header, Dictionary<string, Action> menu, bool isRoot = false)
        {
            var exit = false;

            do
            {
                Console.Clear();

                Console.WriteLine(header);
                Console.WriteLine();

                var counter = 0;
                foreach (var menuItem in menu)
                {
                    Console.WriteLine($"{++counter}. {menuItem.Key}");
                }
                Console.WriteLine($"{++counter}. {(isRoot ? "Exit" : "Back")}");
                Console.Write("\nSelect an option: ");

                var option = 0;

                try
                {
                    option = int.Parse(Console.ReadLine() ?? string.Empty);
                }
                catch (Exception)
                {
                    Console.WriteLine("\nWrong option. Press any key to continue");

                    Console.ReadKey();
                }

                if ((option < 1) || (option > menu.Count + 1))
                {
                    continue;
                }

                Console.Clear();

                if (option == counter)
                {
                    exit = true;
                }
                else
                {
                    var action = menu.ElementAt(option - 1).Value;

                    action();

                    Console.WriteLine("Press any key to continue");
                    Console.ReadLine();
                }
            } while (!exit);
        }

        private static void PrintWaitersMenu()
        {
            PrintContextMenu("Waiters", WaitersContext, () =>
                {
                    Console.WriteLine("Enter waiter's first name");
                    var firstName = Console.ReadLine();

                    Console.WriteLine("\nEnter waiter's last name");
                    var lastName = Console.ReadLine();

                    Console.WriteLine("\nEnter waiter's birthday (YYYY-MM-DD)");
                    var birthdayParts =
                        Console.ReadLine()
                            .Split(new[] {'-'}, StringSplitOptions.RemoveEmptyEntries)
                            .Select(int.Parse)
                            .ToList();
                    var birthday = new Date(birthdayParts[0], birthdayParts[1], birthdayParts[2]);

                    Console.WriteLine("\nEnter waiter's category (1, 2, 3)");
                    WaiterCategory category;
                    Enum.TryParse(Console.ReadLine(), out category);

                    return new Waiter(firstName, lastName, birthday, category);
                },
                name =>
                    waiter =>
                        waiter.FirstName.ToLower().Contains(name.ToLower()) ||
                        waiter.LastName.ToLower().Contains(name.ToLower()));
        }

        private static void PrintDishesMenu()
        {
            PrintContextMenu("Dishes", DishesContext, () =>
                {
                    Console.WriteLine("\nEnter dish's name");
                    var name = Console.ReadLine();

                    Console.WriteLine("\nEnter dish's price");
                    var price = decimal.Parse(Console.ReadLine());

                    Console.WriteLine("\nSelect dish type");
                    PrintTable(DishTypesContext.GetAll());
                    var dishTypeId = int.Parse(Console.ReadLine());
                    var dishType = DishTypesContext.GetAll().ElementAt(dishTypeId - 1);

                    return new Dish(name, price, dishType);
                }, name => dish => dish.Name.ToLower().Contains(name.ToLower()),
                (rangeStart, rangeEnd) => dish => (dish.Price >= rangeStart) && (dish.Price <= rangeEnd));
        }

        private static void PrintDishTypesMenu()
        {
            PrintContextMenu("Dishes Types", DishTypesContext, () =>
            {
                Console.WriteLine("\nEnter dish type name");
                var name = Console.ReadLine();

                return new DishType(name);
            }, name => dishType => dishType.Name.ToLower().Contains(name.ToLower()));
        }

        private static void PrintContextMenu<T>(string header, IContext<T> context, Func<T> newItemProvider,
            Func<string, Func<T, bool>> predicateCreator, Func<decimal, decimal, Func<T, bool>> rangePredicate = null)
        {
            var menuSettings = new Dictionary<string, Action>
            {
                {
                    "Show all", () => { PrintTable(context.GetAll()); }
                },
                {
                    "Add new", () =>
                    {
                        try
                        {
                            context.Add(newItemProvider());
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("\nCan't create new item");
                            Console.WriteLine(ex.Message);
                        }
                    }
                },
                {
                    "Remove", () =>
                    {
                        try
                        {
                            Console.WriteLine("Enter item's Id to remove");
                            var id = int.Parse(Console.ReadLine());

                            var itemToRemove = context.GetAll().ElementAt(id - 1);

                            context.Remove(itemToRemove);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("\nCan't remove a waiter");
                            Console.WriteLine(ex.Message);
                        }
                    }
                },
                {
                    "Find", () =>
                    {
                        Console.WriteLine("Enter item's name");
                        var name = Console.ReadLine();

                        var result = context.GetAll().Where(predicateCreator(name)).ToList();

                        Console.WriteLine("\nSearch results:");
                        PrintTable(result);
                    }
                }
            };

            if (rangePredicate != null)
                menuSettings.Add("Find in range", () =>
                {
                    try
                    {
                        Console.WriteLine("Enter range start");
                        var rangeStart = decimal.Parse(Console.ReadLine());

                        Console.WriteLine("Enter range end");
                        var rangeEnd = decimal.Parse(Console.ReadLine());

                        if (rangeEnd < rangeStart)
                        {
                            throw new ArgumentException("Range start can't be greater than range end");
                        }

                        var result = context.GetAll().Where(rangePredicate(rangeStart, rangeEnd)).ToList();

                        Console.WriteLine("Search results:");
                        PrintTable(result);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Wrong range input");
                        Console.WriteLine(ex.Message);
                    }
                });

            PrintMenu(header, menuSettings);
        }

        private static void PrintManagementMenu()
        {
            PrintMenu("Management", new Dictionary<string, Action>
            {
                {"Waiters", PrintWaitersMenu},
                {"Dishes", PrintDishesMenu},
                {"Dish Types", PrintDishTypesMenu}
            });
        }

        private static void ShowSavedOrders()
        {
            try
            {
                Console.WriteLine("Select a waiter");
                PrintTable(WaitersContext.GetAll());
                var waiterId = int.Parse(Console.ReadLine());
                var waiter = WaitersContext.GetById(waiterId);

                if (waiter == null)
                {
                    Console.WriteLine("\nWaiter not found");
                    return;
                }

                var pathToOrders = $@"{BasePath}\orders\{DailyBalance.GetDate()}\{waiter.FirstName}_{waiter.LastName}";

                var orders = Directory.GetFiles(pathToOrders);

                foreach (var order in orders)
                {
                    Console.Clear();

                    try
                    {
                        Console.WriteLine(File.ReadAllText(order));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("\nCan't read saved order");
                        Console.WriteLine(ex.Message);
                    }

                    Console.WriteLine("Read next? (Y/N)");
                    var response = Console.ReadLine();
                    if (response.ToLower() != "y")
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nCan't read waiter's saved orders");
                Console.WriteLine(ex.Message);
            }
        }

        private static void PrintOrdersMenu()
        {
            PrintMenu("Orders", new Dictionary<string, Action>
            {
                {"New order", CreateOrder},
                {"Show saved orders", ShowSavedOrders}
            });
        }

        private static void CreateOrder()
        {
            try
            {
                Console.WriteLine("Select a waiter");
                PrintTable(WaitersContext.GetAll());
                var waiterId = int.Parse(Console.ReadLine());
                var waiter = WaitersContext.GetById(waiterId);

                var order = new Order(waiter);

                while (true)
                    try
                    {
                        Console.WriteLine("\nSelect a dish");
                        PrintTable(DishesContext.GetAll());
                        var dishId = int.Parse(Console.ReadLine());
                        var dish = DishesContext.GetById(dishId);

                        order.AddDish(dish);

                        Console.WriteLine("\nAdd one more dish? (Y/N)");
                        var response = Console.ReadLine();

                        if (response.ToLower().Trim() != "y")
                        {
                            break;
                        }

                        Console.Clear();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("\nCan't add a dish");
                        Console.WriteLine(ex.Message);
                    }

                Console.WriteLine("\nYour order is:");
                Console.WriteLine(order);

                Console.WriteLine("Print to file? (Y/N)");
                var printResponse = Console.ReadLine();

                if (printResponse.ToLower().Trim() == "y")
                {
                    var pathToPrinterOrder = OrderPrinter.PrintOrderToFile(order, BasePath);

                    Console.WriteLine($"\nOrder was saved here: {pathToPrinterOrder}");
                }

                DailyBalance.AddOrder(order);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nCan't create an order");
                Console.WriteLine(ex.Message);
            }
        }

        private static void PrintStatisticsMenu()
        {
            PrintMenu("Statistics", new Dictionary<string, Action>
            {
                {
                    "Daily total", () =>
                    {
                        Console.WriteLine("Daily total is:");
                        DailyBalance.PrintStats(orders => { Console.WriteLine($"{orders.Sum(i => i.GetTotal()):c}"); });
                    }
                },
                {
                    "Daily tips for waiters", () =>
                    {
                        Console.WriteLine("Daily tips for waiters:");

                        DailyBalance.PrintStats(orders =>
                        {
                            if (!orders.Any())
                            {
                                Console.WriteLine("\nNo data to display");

                                return;
                            }

                            Console.WriteLine("\nWaiter\tTips");

                            foreach (
                                var waiterTips in
                                orders.GroupBy(i => i.Waiter).ToDictionary(i => i.Key, j => j.Sum(o => o.GetTips())))
                            {
                                Console.WriteLine($"{waiterTips.Key}\t{waiterTips.Value:C}");
                            }
                        });
                    }
                },
                {
                    "Best waiter of the day", () =>
                    {
                        Console.WriteLine("Best waiter of the day is:");

                        DailyBalance.PrintStats(orders =>
                        {
                            var bestWaiter = orders.GroupBy(i => i.Waiter)
                                .Select(i => new {Waiter = i.Key, Total = i.Sum(o => o.GetTotal())})
                                .OrderByDescending(i => i.Total).FirstOrDefault();

                            Console.WriteLine(bestWaiter != null
                                ? $"{bestWaiter.Waiter} ({bestWaiter.Total:c})"
                                : "Unknown");
                        });
                    }
                },
                {
                    "Waiters' statistics", () =>
                    {
                        Console.WriteLine("Waiters' statistics");

                        var stats = DailyBalance.GetWaitersStats();

                        if (!stats.Any())
                        {
                            Console.WriteLine("\nNo data to display");

                            return;
                        }

                        Console.WriteLine("\nWaiter\tStats");

                        foreach (var waiterStats in stats)
                        {
                            Console.WriteLine($"{waiterStats.Key}\t{waiterStats.Value:c}");
                        }
                    }
                }
            });
        }

        private static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            PrintMenu("Main menu", new Dictionary<string, Action>
            {
                {"Management", PrintManagementMenu},
                {"Orders", PrintOrdersMenu},
                {"Statistics", PrintStatisticsMenu}
            }, true);
        }
    }
}