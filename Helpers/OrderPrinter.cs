﻿using System;
using System.IO;
using CourseWork.Models;

namespace CourseWork.Helpers
{
    internal static class OrderPrinter
    {
        public static string PrintOrderToFile(IOrder order, string basePath)
        {
            var path = $@"{basePath}\orders\{order.OrderDate}\{order.Waiter.FirstName}_{order.Waiter.LastName}";

            Directory.CreateDirectory(path);

            var fileName = Guid.NewGuid().ToString();

            var pathToFile = $@"{path}\{fileName}.txt";

            File.WriteAllText(pathToFile, order.ToString());

            return pathToFile;
        }
    }
}