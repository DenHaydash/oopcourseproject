﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CourseWork.Helpers
{
    internal static class SerializationHelper
    {
        public static void SaveDataToBinaryFile<T>(string path, T data)
        {
            using (var stream = File.Open(path, FileMode.Create))
            {
                var bformatter = new BinaryFormatter();

                bformatter.Serialize(stream, data);
            }
        }

        public static T ReadDataFromBinaryFile<T>(string path)
        {
            try
            {
                using (var stream = File.Open(path, FileMode.Open))
                {
                    var bformatter = new BinaryFormatter();

                    return (T) bformatter.Deserialize(stream);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't read saved binary file");
                Console.WriteLine(ex.Message);

                return default(T);
            }
        }
    }
}