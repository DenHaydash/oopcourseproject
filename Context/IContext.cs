﻿using System;
using System.Collections.Generic;

namespace CourseWork.Context
{
    internal interface IContext<T> : IObservable<List<T>>
    {
        IList<T> GetAll();
        T GetById(int id);
        void Add(T item);
        void Remove(T item);
    }
}