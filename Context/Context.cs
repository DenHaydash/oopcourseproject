﻿using System;
using System.Collections.Generic;

namespace CourseWork.Context
{
    internal sealed class Context<T> : IContext<T> where T: class
    {
        private readonly List<IObserver<List<T>>> _observers;

        private readonly List<T> _items;

        public Context(List<T> items = null)
        {
            _items = items ?? new List<T>();
            _observers = new List<IObserver<List<T>>>();
        }

        public IList<T> GetAll()
        {
            return _items.AsReadOnly();
        }

        public T GetById(int id)
        {
            try
            {
                return _items[id - 1];
            }
            catch (ArgumentOutOfRangeException)
            {
                return null;
            }
            catch (IndexOutOfRangeException)
            {
                return null;
            }
        }

        public void Add(T item)
        {
            _items.Add(item);

            NotifyObservers();
        }

        public void Remove(T item)
        {
            _items.Remove(item);

            NotifyObservers();
        }

        private void NotifyObservers()
        {
            foreach (var observer in _observers)
            {
                observer.OnNext(_items);
            }
        }

        public IDisposable Subscribe(IObserver<List<T>> observer)
        {
            _observers.Add(observer);

            return new Unsubscriber<List<T>>(_observers, observer);
        }
    }
}
