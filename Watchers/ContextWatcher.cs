﻿using System;
using CourseWork.Helpers;

namespace CourseWork.Watchers
{
    internal class ContextWatcher<T> : IObserver<T>
    {
        private readonly string _saveToFilePath;

        public ContextWatcher(string saveToFilePath)
        {
            _saveToFilePath = saveToFilePath;
        }

        public void OnNext(T value)
        {
            try
            {
                SerializationHelper.SaveDataToBinaryFile(_saveToFilePath, value);

                Console.WriteLine("Saved");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't save updated context");
                Console.WriteLine(ex.Message);
            }
        }

        public void OnError(Exception error)
        {
            Console.WriteLine($"Error: {error.Message}");
        }

        public void OnCompleted()
        {
        }
    }
}